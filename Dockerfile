ARG nginx_version=1.20.0
ARG luajit_version=2.0.4
ARG LUAJIT_LIB=/usr/local/lib
ARG LUAJIT_INC=/usr/local/include/luajit-2.0

FROM debian:9 as buildr
WORKDIR /opt
ARG nginx_version
ARG luajit_version
ARG LUAJIT_LIB
ARG LUAJIT_INC
RUN apt update && \
    apt install -y gcc make wget libpcre3-dev zlib1g-dev git && \
    wget http://nginx.org/download/nginx-${nginx_version}.tar.gz && \
    tar -xzvf nginx-${nginx_version}.tar.gz && \
    git clone https://github.com/openresty/lua-nginx-module && \
    wget http://luajit.org/download/LuaJIT-${luajit_version}.tar.gz && \
    tar -xzvf LuaJIT-${luajit_version}.tar.gz && \
    cd LuaJIT-${luajit_version} && make && make install && \
    cd ../nginx-${nginx_version} && \
    ./configure --add-module=../lua-nginx-module --with-ld-opt="-Wl,-rpath,/opt/LuaJIT-${luajit_version}" && \
    make && \
    make install


FROM debian:9
ARG luajit_version
ARG LUAJIT_LIB
COPY --from=buildr /usr/local/nginx/ /usr/local/nginx
COPY --from=buildr /usr/local/lib/libluajit-5.1.a /usr/local/lib/
COPY --from=buildr /usr/local/lib/libluajit-5.1.so /usr/local/lib/
COPY --from=buildr /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
COPY --from=buildr /usr/local/lib/libluajit-5.1.so.${luajit_version} /usr/local/lib/
ENV LD_LIBRARY_PATH=${LUAJIT_LIB}
WORKDIR /usr/local/nginx/sbin
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]